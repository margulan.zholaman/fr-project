
# Система для рассылки

## Описание

Это автоматическая система рассылки. Вы можете создавать рассылки и планировать их. Приложение использует apscheduler для планирования задач.

  
  

## Установка

1) git clone https://gitlab.com/margulan.zholaman/fr-project.git

2) cd fr

3) python fr/manage.py migrate

4) python fr/manage.py createsuperuser` & create a django admin account for you to use locally

5) python fr/manage.py runserver

6) Используйте http://127.0.0.1:8000/docs/ чтобы увидеть автоматическую документацию на swagger

## Endpoints

- /clients

1) get -> returns list of all clients

2) post-> creates new client


- /clients/{id}

1) get -> returns client with given id

2) put -> updates client with given id

3) delete -> deletes client with given id

- /mailing

1) get -> returns list of all mailings

2) post-> creates new mailing

- /mailing/{id}

1) get -> returns mailing with given id

2) put -> updates mailing with given id

3) delete -> deletes mailing with given id

- /messages

1) get -> returns list of all messages

2) post-> creates new messages

- /messages/{id}

1) get -> returns message with given id

2) put -> updates message with given id

3) delete -> deletes message with given id

- /stats

1) get -> return some statistics about mailings


- /stats/{id}

1) get ->  return some statistics about certain mailing


## Дополнительные задания

Были выполнены такие дополнительные задания как:

- организовать тестирование написанного кода
- сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. 
- реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
