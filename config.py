from dotenv import load_dotenv
import os


load_dotenv()

TOKEN = os.environ.get('TOKEN')
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_PORT = os.environ.get('EMAIL_PORT')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
EMAIL_ADMIN = os.environ.get('EMAIL_ADMIN')
