from django.apps import AppConfig


class MailingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing'

    def ready(self):
        from . import signals

        from .scheduler import scheduler, sending_stats
        scheduler.add_job(sending_stats, 'interval', days=24)
