from django.shortcuts import render
from .models import Mailing, Client, Message
from rest_framework import viewsets
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer, MailingStatsSerializer
from rest_framework.response import Response
# Create your views here.


class MailingView(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class ClientView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageView(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    http_method_names = ['get']


class MailingStats(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingStatsSerializer
    http_method_names = ['get']
