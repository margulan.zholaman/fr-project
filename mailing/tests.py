from rest_framework.test import APITestCase
from rest_framework import status
import json
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from .models import Client, Mailing, Tag
import pytz
# Create your tests here.


class ModelTest(APITestCase):
    def test_dates(self):
        url = 'http://localhost/mailing/'
        data = {
            "start_at": "2023-05-13T14:19:00+06:00",
            "content": "test",
            "phone_code_filter": "8",
            "finish_at": "2023-05-14T14:20:00+06:00",
            "tag_filter": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_dates_failed(self):
        url = 'http://localhost/mailing/'
        data = {
            "start_at": "2023-05-13T14:20:00+06:00",
            "content": "test",
            "phone_code_filter": "8",
            "finish_at": "2023-05-13T14:15:00+06:00",
            "tag_filter": []
        }
        with self.assertRaises(ValidationError):
            self.client.post(url, json.dumps(
                data), content_type='application/json')

    def test_phone_number_validation(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681",
            "phone_code": "8",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_phone_number_length_validation_failed(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "74727476811",
            "phone_code": "8",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_phone_number_signs_validation_failed(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681d",
            "phone_code": "8",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_phone_code_validation(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681",
            "phone_code": "7",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_phone_code_length_validation_failed(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681",
            "phone_code": "7777",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_phone_code_signs_validation_failed(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681",
            "phone_code": "+7",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class LogicTest(APITestCase):
    def setUp(self):
        self.test_tag = Tag.objects.create(tag_name='customer')
        self.test_client = Client.objects.create(
            phone_number='7777777777', phone_code='7', time_zone='gmt+6')
        self.test_client.tag.add(self.test_tag.id)
        self.test_mailing = Mailing.objects.create(start_at=datetime.utcnow().replace(
            tzinfo=pytz.UTC), content='test', finish_at=datetime.utcnow().replace(tzinfo=pytz.UTC) + timedelta(hours=1))

    def test_client_create(self):
        url = 'http://localhost/clients/'
        data = {
            "phone_number": "7472747681",
            "phone_code": "8",
            "time_zone": "gmt+6",
            "tag": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_client_update(self):
        url = f'http://localhost/clients/{self.test_client.id}/'
        data = {
            "id": self.test_client.id,
            "phone_number": "7472747682"
        }
        response = self.client.patch(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_client_delete(self):
        url = f'http://localhost/clients/{self.test_client.id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_mailing_create(self):
        url = 'http://localhost/mailing/'
        data = {
            "start_at": "2023-05-13T14:19:00+06:00",
            "content": "test",
            "phone_code_filter": "8",
            "finish_at": "2023-05-14T14:20:00+06:00",
            "tag_filter": []
        }
        response = self.client.post(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_mailing_update(self):
        url = f'http://localhost/mailing/{self.test_mailing.id}/'
        data = {
            "content": "test"
        }
        response = self.client.patch(url, json.dumps(
            data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_mailing_delete(self):
        url = f'http://localhost/mailing/{self.test_mailing.id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_stats(self):
        url = 'http://localhost/stats/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_stats_certain_mailing(self):
        url = f'http://localhost/stats/{self.test_mailing.id}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
