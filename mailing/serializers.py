from rest_framework import serializers
from .models import Mailing, Client, Message, Tag


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class MailingStatsSerializer(serializers.ModelSerializer):
    messages_sent = serializers.SerializerMethodField()
    messages_to_be_sent = serializers.SerializerMethodField()
    total_messages_count = serializers.SerializerMethodField()

    class Meta:
        model = Mailing
        fields = (
            'id',
            'messages_sent',
            'messages_to_be_sent',
            'total_messages_count',
        )

    def get_messages_sent(self, obj):
        return obj.messages.filter(status=True).count()

    def get_messages_to_be_sent(self, obj):
        return obj.messages.filter(status=False).count()

    def get_total_messages_count(self, obj):
        return obj.messages.count()
