from django.urls import path, include
from .views import MailingView, ClientView, MessageView, MailingStats
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"mailing", MailingView, basename="mailing")
router.register(r"clients", ClientView,  basename="client")
router.register(r"messages", MessageView, basename="messages")
router.register(r"stats", MailingStats, basename="mailingstats")


urlpatterns = [
    path('', include(router.urls))
]
