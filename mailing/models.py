from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
# Create your models here.


class Tag(models.Model):
    tag_name = models.CharField(max_length=50)

    def __str__(self):
        return self.tag_name


class Mailing(models.Model):
    start_at = models.DateTimeField()
    content = models.TextField()
    phone_code_filter = models.CharField(
        max_length=3, validators=[RegexValidator('^[0-9]*$')], blank=True)
    tag_filter = models.ManyToManyField(
        Tag, related_name='mailing', blank=True)
    finish_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        if self.finish_at < self.start_at:
            raise ValidationError(
                "The finish_at cannot be in earlier than start_at!")
        super(Mailing, self).save(*args, **kwargs)


class Client(models.Model):
    phone_number = models.CharField(max_length=10, validators=[
                                    RegexValidator('^[0-9]*$')])
    phone_code = models.CharField(max_length=3, validators=[
                                  RegexValidator('^[0-9]*$')])
    tag = models.ManyToManyField(Tag, related_name='clients', blank=True)
    time_zone = models.CharField(max_length=30)


class Message(models.Model):
    sent_at = models.DateTimeField(blank=True, null=True)
    status = models.BooleanField(default=False)
    mailing = models.ForeignKey(
        Mailing, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages")
