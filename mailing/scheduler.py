from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta
import pytz
import requests
import time
from rest_framework import status
from config import TOKEN, EMAIL_HOST, EMAIL_ADMIN
from django.core.mail import send_mail
from datetime import date
from django.utils import timezone
from .models import Client, Message, Mailing

token = TOKEN
headers = {"Authorization": f"Bearer {token}"}


scheduler = BackgroundScheduler()

scheduler.start()


def send_message(mailing):
    now = datetime.utcnow().replace(tzinfo=pytz.UTC)
    if now > mailing.finish_at:
        return
    clients = Client.objects.all()
    if mailing.phone_code_filter:  # Filter clients by phone code
        clients = clients.filter(phone_code=mailing.phone_code_filter)
    if mailing.tag_filter.all():  # Filter clients by tag
        new_clients = Client.objects.none()
        for tag in mailing.tag_filter.all():
            new_clients = (new_clients | clients.filter(
                tag__id=tag.id).distinct()).distinct()
        clients = new_clients
    for client in clients:
        if Message.objects.filter(mailing=mailing, client=client, status=False).exists():
            new_message = Message.objects.get(mailing=mailing, client=client)
        else:
            new_message = Message.objects.create(
                mailing=mailing, client=client)
        url = f'https://probe.fbrq.cloud/v1/send/{new_message.id}'
        data = {
            'id': new_message.id,
            'phone': int(client.phone_number),
            'text': mailing.content
        }
        response = requests.post(url, json=data, headers=headers)
        if response.status_code == status.HTTP_200_OK:
            print("Successfull sending")
            new_message.sent_at = now
            new_message.status = True
            new_message.save()
        else:
            print("Adding mailing again to send messages")
            # Repeat sending 1 hour later
            scheduler.add_job(
                send_message, 'date', run_date=mailing.start_at + timedelta(hours=1), args=[mailing])


def sending_stats():
    mailing_processed = Mailing.objects.filter(start_at__gte=timezone.now().replace(
        hour=0, minute=0, second=0), start_at__lte=timezone.now().replace(hour=23, minute=59, second=59)).count()
    messages_sent = Message.objects.filter(sent_at__gte=timezone.now().replace(
        hour=0, minute=0, second=0), sent_at__lte=timezone.now().replace(hour=23, minute=59, second=59), status=True).count()
    messages_not_sent = Message.objects.filter(sent_at__gte=timezone.now().replace(
        hour=0, minute=0, second=0), sent_at__lte=timezone.now().replace(hour=23, minute=59, second=59), status=False).count()
    tmp = 1
    title = 'Статистика рассылок за сегодня'
    body = f'Кол-во обработанных рассылок: {mailing_processed}\nКол-во отправленных сообщений: {messages_sent}\nКол-во не отправленных сообщений: {messages_not_sent}\n'

    send_mail(
        title,
        body,
        EMAIL_HOST,
        [EMAIL_ADMIN],
        fail_silently=False
    )
