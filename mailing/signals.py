from .models import Mailing, Message, Client
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime, timedelta
import pytz
from .scheduler import scheduler, send_message


@receiver(post_save, sender=Mailing)
def schedule_mailing(sender, instance, created, **kwargs):
    if created:
        now = datetime.utcnow().replace(tzinfo=pytz.UTC)
        if instance.start_at < now:
            instance.start_at = now
        scheduler.add_job(send_message, 'date',
                          run_date=instance.start_at, args=[instance])
